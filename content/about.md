+++
title = "About Me"
slug = "about"
date = "2022-02-22"
author = "Aayush Shah Kanu"
cover = "images/cover.jpg"
+++

Hi, I am Aayush.
I am a computer engineering undergraduate studying at the [Institute of Engineering](https://ioe.edu.np/), [Pulchowk Campus](https://pcampus.edu.np/).

### Interests

- Artificial Intelligence
- Machine Learning
- Image Processing
- Natural Language Processing
- Blockchain
- Web Development

