+++
title = "CV: Aayush Shah Kanu"
slug = "cv"
date = "2022-02-22"
author = "Aayush Shah Kanu"
tags = ["cv", "resume", "portfolio"]
+++

#### Primary Information
**Name:** Aayush Shah Kanu\
**Employment Status:** Student\
**College:** Pulchowk Campus, IOE\
**Undergraduate:** Bachelor in Computer Engineering\
**Year:** Third Year, Sixth Semester\
**Address:**
- Primary: Biratnagar-3, Morang 
- Secondary: Pulchowk, Lalitpur (current)
- Permanent: Barah-2, Sunsari

**Email:**
- aayushshah196@gmail.com

---

#### Skills:

- **Programming Languages** : Python, C++, C and JavaScript
- **Machine Learning** : Numpy, Pandas, Scikit-Learn TensorFlow, Pytorch
- **Web Development** : Reactjs, Expressjs, Flask and Django
- **Databases** : SQLite3, Postgres, MongoDB
- **Version Control** : Git

#### Achievements and Awards:
- **Ncell Scholarship, 2020**,
Ncell under its Corporate Social Responsibility(CSR) initiated scholarship provided to outstanding students.

#### Work Experience:

- Freelancer at **[Upwork](https://www.upwork.com/freelancers/~01257eab052741ec05)**


#### Academic And Non Academic Projects:


- **[Anubadak: interpreter implemented in C++](https://github.com/Atomnp/Anubadak)**

  - Academic Project for Object Oriented Programming
  - Simple interpreter for `B` like language
  - Uses common compiler design principles like lexical analysis, semantic analysis, Abstract Syntax Tree(AST)
  - Translates Code into equivalent C++ code 


- **[DSA Visualizer](https://github.com/Aayushshah196/DataStructure-Algorithms)**

  - Visualization of different Data Structure and Algorithms
  - Include visualization for sorting (Quicksort,Mergesort,Shellshort,Insertionsort, Selectionsort, Radixsort etc.)
  - Includes visualization for Graph Algorithms ( Prism's Algorithm, Kruskal's Algorithm, Breadth First Search, Depth First Search)
  - Implemented in SFML using C++
  

- **[3D Clock Tower visualization](https://github.com/ADI13579/Opengl-Project)**

  - Academic Project for Computer Graphics
  - Implemented using C++
  - Renderer created from scratch which can render any 3D model
  - implements Gourad shading and Phong shading
  - implements Phong illumination model
  - Blender for 3D model


#### Certifications

- [Deep Learning Specialization, Coursera](https://coursera.org/share/ae6b046ba7dfdb6a0141dbd3001d99c2)
- [DeepLearning.AI TensorFlow Developer Specialization, Coursera](https://coursera.org/share/192007cd3405ac570fd76216389fee4e)
- [AI for Medicine Specialization, Coursera](https://coursera.org/share/a13af8aedbe1cd072c3de65f6130eeb6)
- [Build Basic Generative Adversarial Networks (GANs), Coursera](https://coursera.org/share/81b77842ce2033dd8084c20adedf4d7b)
- [Natural Language Processing with Classification and Vector Spaces, Coursera](https://coursera.org/share/9e66f2d7cac89a081fab9b2ba8abad07)

---
#### Links:

- **[Personal Website](https://aayushshah.com.np/)**
- **[GitHub Profile](https://github.com/aayushshah196)**
- **[LinkedIn](https://www.linkedin.com/in/aayush-shah-kanu-3b04401a2)**
